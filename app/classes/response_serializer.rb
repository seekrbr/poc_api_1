class ResponseSerializer < ActiveModel::Serializer
	attributes :http_code, :error_code, :body

	def http_code
		object.http_code
	end

	def error_code 
		object.error_code
	end

	def body
		object.body
	end
end