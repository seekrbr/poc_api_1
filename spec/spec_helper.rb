require 'simplecov'
require 'simplecov-rcov'

SimpleCov.formatters = [
    SimpleCov::Formatter::RcovFormatter,
    SimpleCov::Formatter::HTMLFormatter 
]

SimpleCov.start 'rails' do 
  add_filter "/test/"
  add_filter "/spec/"
  add_filter "/config/"
  add_filter "/db/"
  add_filter "/script/"

  add_group "Models", "app/models"
  add_group "Controllers", "app/controllers"
  add_group "Classes", "app/classes"
  
  add_filter do |source_file|
    source_file.lines.count < 5
  end

end

RSpec.configure do |config|

  #config.color_enabled = true
  config.tty = true
  config.formatter = :documentation
  #config.include FactoryGirl::Syntax::Methods

  # rspec-expectations config goes here. You can use an alternate
  # assertion/expectation library such as wrong or the stdlib/minitest
  # assertions if you prefer.
  config.expect_with :rspec do |expectations|
    # This option will default to `true` in RSpec 4. It makes the `description`
    # and `failure_message` of custom matchers include text for helper methods
    # defined using `chain`, e.g.:
    #     be_bigger_than(2).and_smaller_than(4).description
    #     # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #     # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end
end
