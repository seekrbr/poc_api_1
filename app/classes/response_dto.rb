class ResponseDto
	alias :read_attribute_for_serialization :send
	
	@http_code
	@error_code
	@body

	def error_code
		@error_code
	end

	def error_code=(error_code)
		@error_code = error_code
	end

	def http_code
		@http_code
	end

	def http_code=(http_code)
		@http_code = http_code
	end

	def body
		@body
	end

	def body=(body)
		@body = body
	end

	def initialize
		@http_code = 200
		@error_code = 0
		@body = nil
	end
end