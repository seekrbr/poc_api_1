class Api::V1::RestController < ApplicationController

	def index
		
		user = User.new
		user.name = "Ricardo Seekr"
		user.age = 34

		response = ResponseDto.new
		response.body = user
					
		render json: ResponseSerializer.new(response)
	end
end
